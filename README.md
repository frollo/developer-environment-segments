# Dev env segments

Powerline segments to handle different development environments.

## Dependecies

You'll need [powerline](https://github.com/powerline/powerline/) installed on your system.

## Installation

Clone this repository and run `pip3 install ./` inside it.

## Segments

Currently supported segments:
  * `gcloud.project` (requires Google Cloud SDK): displays the ID of the current Google project
  * `sdkman.java`: displays the version number of the current Java executable
  * `sdkman.maven`: displays the version number of the current Maven executable
  * `nvm.node`: displays the version number of the current Node executable
  * `nvm.npm`: displays the version number of the current NPM executable
