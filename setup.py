import setuptools

setuptools.setup(
    name="dev_env_segments",
    version="0.1.0",
    author="Lorenzo Rossi",
    description="Custom powerline shell segments for developers",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
)
