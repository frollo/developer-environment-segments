# vim:fileencoding=utf-8:noet
from powerline.lib.shell import run_cmd

def project(pl):
    project = run_cmd(pl, ["gcloud", "config", "get-value", "project"]).strip()
    return [{
      'contents': "(gcp) " + project
      }]
