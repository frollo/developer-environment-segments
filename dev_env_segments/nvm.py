# vim:fileencoding=utf-8:noet
from powerline.lib.shell import run_cmd

def node(pl):
    '''
    Return current node version
    '''
    version = run_cmd(pl, ["node", "--version"])[1:]
    return [{
      'contents': "(n) " + version
      }]

def npm(pl):
    '''
        Return the current NPM version
    '''
    version = run_cmd(pl, ["npm", "--version"])
    return [{
        "contents" : "(npm) " + version
    }]
