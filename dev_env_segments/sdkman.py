# vim:fileencoding=utf-8:noet
from powerline.lib.shell import run_cmd

from random import randint

def java(pl):
    '''
        Returns current java version
    '''
    return [{
        'contents' : "(j) "  + run_cmd(pl, ['java', '--version']).split(" ")[1]
    }]

def maven(pl):
    '''
        Returns current maven version
    '''
    version = run_cmd(pl, ['mvn', '--version']).split(" ")[2]
    return [{
        'contents' : "(mvn) " + version
    }]
